Remove-Item -Recurse -ErrorAction Ignore dist
New-Item -Type Directory dist
jspm bundle-sfx app/main dist/app.js
./node_modules/.bin/uglifyjs dist/app.js -o dist/app.min.js
./node_modules/.bin/html-dist index.html --remove-all --minify --insert app.min.js --output dist/index.html